package dominio;

public class Sucesion {
	private int n;
	public static int calcularFibonacci(int n) {
		return (n==0 || n==1)?1:calcularFibonacci(n - 1) + calcularFibonacci(n - 2);
	}
}
