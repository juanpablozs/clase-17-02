public class Principal {
	public static void hanoi(int n, int origen, int auxiliar, int destino) {
		if(n==1) mover(origen, destino);
		else {
			if (n>1) {
				mover(origen, auxiliar);
				hanoi(n - 1, origen, auxiliar, destino);
			}
	
		}
	}

	public static void mover(int a, int b){
		System.out.println("Mover de " + a + " a " +b);
	}
	
	public static void main(String[] args)
	{
		hanoi(4, 1, 2, 3);
	}  
	 
}	
