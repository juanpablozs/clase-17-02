limpiar:
	rm -rf bin
	mkdir bin
compilar:limpiar
	find src -name "*.java" | xargs javac -cp bin -d bin
ejecutar:compilar
	java -cp bin aplicacion.Principal
